package com.capgemini.osce.greenhouse.lambdatest;

import com.capgemini.osce.greenhouse.api.sensor.SensorSchema;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

public class Main implements Runnable {

    private static final String REQUEST_ID_HEADER = "Lambda-Runtime-Aws-Request-Id";

    private final URI nextMessageUrl;
    private final String responseUrlPattern;
    private final String errorUrl;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final HttpClient client = HttpClientBuilder.create().build();

    private Main(String rootUrl) {
        nextMessageUrl = URI.create(String.format("http://%s/2018-06-01/runtime/invocation/next", rootUrl));
        errorUrl = String.format("http://%s/2018-06-01/runtime/init/error", rootUrl);
        responseUrlPattern = String.format("http://%s/2018-06-01/runtime/invocation/%%s/response", rootUrl);
    }

    public static void main(String[] args) {
        String url = System.getenv("AWS_LAMBDA_RUNTIME_API");
        new Main(url).run();
    }

    @Override
    @SuppressWarnings("squid:S2189")
    public void run() {
        boolean running = true;
        HttpGet request = new HttpGet(nextMessageUrl);
        while (running) {
            try {
                System.out.println("Loop");
                HttpResponse message = client.execute(request);
                System.out.println("Response : " + message.getStatusLine().getStatusCode());
                if (message.getStatusLine().getStatusCode() == 200) {
                    System.out.println("Has 200 - parsing");
                    SensorSchema data = parseRequest(message);
                    System.out.println(data.toString());
                    sendResponse(message, "OK");
                } else {
                    message.getEntity().getContent().close();
                }
                System.out.println("OK Loop");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Throwable e) {
                e.printStackTrace();
                throw e;
            }
            request.reset();
            System.out.println("End Loop");
        }
        System.out.println("Exit Loop");
    }

    private SensorSchema parseRequest(HttpResponse response) throws IOException {
        try (InputStream stream = response.getEntity().getContent()) {
            return objectMapper.readerFor(SensorSchema.class).readValue(stream);
        }
    }

    private void sendResponse(HttpResponse request, String body) throws IOException {
        String url = String.format(responseUrlPattern, request.getFirstHeader(REQUEST_ID_HEADER).getValue());
        System.out.println("Sending to " + url);
        HttpPost request1 = new HttpPost(url);
        request1.setEntity(new StringEntity(body));
        HttpResponse response = client.execute(request1);
        request1.reset();
        System.out.println("Response: " + response.getStatusLine().getStatusCode() + " : " + response.getStatusLine().getReasonPhrase());
    }
}
