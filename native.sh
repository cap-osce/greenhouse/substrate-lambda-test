#!/usr/bin/env bash
native-image \
    --allow-incomplete-classpath \
    --enable-http \
    --enable-https \
    --enable-url-protocols=http,https \
    --enable-all-security-services \
    -Djava.net.preferIPv4Stack=true \
    -H:+ReportUnsupportedElementsAtRuntime \
    -H:ReflectionConfigurationFiles=src/main/build/reflect.json \
    --no-server \
    -jar \
    target/*-jar-with-dependencies.jar \
    ./bootstrap