FROM openjdk:11-jre-slim
VOLUME /tmp
ADD /target/greenhouse-api-*.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
